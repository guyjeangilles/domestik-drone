# Domestik Drone
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

An autonomous vacuum robot modeled after the [domestik-drone, aka "space roomba"](https://warframe.fandom.com/wiki/Domestik_Drone) from the free-to-play role-playing game [Warframe.](https://www.warframe.com/landing).

<img src="https://static.wikia.nocookie.net/warframe/images/e/e4/DomestikDrone.png/revision/latest/scale-to-width-down/512?cb=20200424135450" alt="In-game domestik drone" width="400
"/>
<img src="media/scale.JPG" alt="Built domestik-drone" width="400"/>
![](media/domestik-dron-demo-master-2022_05-15.mp4)

I am not affiliated with Warframe or it's developer, Digital Extremes.
The code is released under GPL3+.

## Table of Contents
[[_TOC_]]

## Parts List
Check the bill of materials (BOM) in the root directory for specifics about price and vendors.
### Electronics
- Raspberry Pi 3 A+
- DC Motor (4x)
- Wheel (4x)
- DC Fan
- Powerboost 1000c
- Lithium ion battery
- TB6612 breakout board (3x)
- 15V step up converter
- RPLidar A1M8 hardware rev 7

### Hardware
- Metal Chassis
- M2.5 x 0.45  5mm screw (24x)
- M2.5 x 0.45 10 mm screw (14x)
- M2.5 x 0.45 35mm screw (4x)
- M2.5 x 0.45 hex nut (18x)
- M2.5 x 0.45mm 31mm Male-Female Standoff (4x)
- M2.5 x 0.45 37mm Female Standoff (10x)
- M2.5 washer (8x)
- Rubber gasket
- Worm drive clamp (2x)
- Vacuum hose

### Cosmetics
I used Rust-Oleum spray paint and E6000 for the paint and glue respectively.
You can use whatever you want. I also used Inland PLA+ for all the parts except for the clear LED windows, which are Sunlu Clear PLA.

## Schematic
Not shown in the schematic are the cable connections.
I powered the Pi via USB to the Powerboost 1000c, but it is possible to power the Pi directly via the 5V pin.
The code *requires* the RPLidar to be connected to the Pi via USB.

You can solder the parts together however you want, but this shield can reduce the workload: https://www.adafruit.com/product/2310.

![Electrical schematic](media/schematic.png)

## Software Troubleshooting
The software was tested with the following:
- Raspian GNU/Linux Version 11.2
- g++ (Raspbian++ (Raspbian 10.2.1-6+rpi1) 10.2.1 20210110)
- GNU Make 4.3 built for arm-unknown-linux-gnueabihf
- Slamtec firmware version 1.29
- Slightly modified version of Slamtec SDK 1.12.0, use the setup script in root for the url.
- BCM2835 C Library v1.71

To make PWM work, the BCM2835 Library needs access to `/dev/gpiomem`. Running compiled code with `sudo` is the easiest way to grant the library access.
Also, the audio on the Raspberry Pi must be disabled for PWM to work. Do this by editing `/boot/config.txt` and rebooting.

## Note on the STLs
1. These parts do not have the same aspect ratio of the domestik drone in-game.
I wanted to incorporate the RPLidar into the cosmetic design and I also wanted to minimize the parts I had to split due to my printer's size.
For this reason, the height of the cosmetic parts are smaller than their width and depth compared to the domestik drone in-game.

1. The parts I split have the file name `<PART_NUMBER>-<LETTER>-<NAME>.stl`. I included their non-split file with the name `<PART_NUMBER>-<NAME>.stl`.
For example, `08-A-cosmetic-center-top.stl` and `08-B-cosmetic-center-top.stl` are meant to be glued together to form `08-cosmetic-center-top.stl`.
If you have a big enough printer, just print the full `08-cosmetic-center-top.stl` and move forward.
Or, split `08-cosmetic-center-top.stl` differently than I split it.

## Gallery
![Domestik Drone Front](media/beauty_shot_front.JPG)
![Domestik Drone 3/4 Left](media/beauty_shot_iso_left.JPG)
![Domestik Drone 3/4 Right](media/beauty_shot_iso_right.JPG)
![Domestik Drone Left](media/beauty_shot_left.JPG)
![Domestik Drone Rear](media/beauty_shot_rear.JPG)
![Domestik Drone Rear Left](media/beauty_shot_rear_left.JPG)
![Domestik Drone Right](media/beauty_shot_right.JPG)
![Chassis 3/4 Left](media/mechatronics_iso_left.JPG)
![Chassis 3/4 Right](media/mechatronics_iso_right.JPG)
![Chassis Left](media/mechatronics_left.JPG)
![Chassis 3/4 Rear Left](media/mechatronics_iso_rear_left.JPG)
