#!/usr/bin/bash

set -Eeuo pipefail

# RPLidar SDK
cd ..
git clone https://github.com/xayhewalo/rplidar_sdk.git
cd rplidar_sdk
git checkout 816eac1
make

# BCM2835 C library
cd ..
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.71.tar.gz
tar -xvzf bcm2835-1.71.tar.gz
rm bcm2835-1.71.tar.gz
cd bcm2835-1.71/
./configure
make
sudo make check
sudo make install

# Move to project directory
cd ../domestik-drone
