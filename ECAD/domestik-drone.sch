EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Autonomous Vacuum Robot"
Date "2022-05-16"
Rev "Rev 1"
Comp ""
Comment1 "Creative Commons SA 4.0"
Comment2 "Guyrandy Jean-Gilles"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Raspberry_Pi_2_3 J1
U 1 1 62830120
P 9300 2400
F 0 "J1" H 8600 3900 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 8900 3800 50  0000 C CNN
F 2 "" H 9300 2400 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 9300 2400 50  0001 C CNN
	1    9300 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 6283555F
P 9250 4100
F 0 "#PWR08" H 9250 3850 50  0001 C CNN
F 1 "GND" H 9255 3927 50  0000 C CNN
F 2 "" H 9250 4100 50  0001 C CNN
F 3 "" H 9250 4100 50  0001 C CNN
	1    9250 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 3700 8900 3950
Wire Wire Line
	8900 3950 9000 3950
Wire Wire Line
	9250 3950 9250 4100
Wire Wire Line
	9000 3700 9000 3950
Connection ~ 9000 3950
Wire Wire Line
	9000 3950 9100 3950
Wire Wire Line
	9100 3700 9100 3950
Connection ~ 9100 3950
Wire Wire Line
	9100 3950 9200 3950
Wire Wire Line
	9200 3700 9200 3950
Connection ~ 9200 3950
Wire Wire Line
	9200 3950 9250 3950
Wire Wire Line
	9600 3700 9600 3950
Wire Wire Line
	9600 3950 9500 3950
Connection ~ 9250 3950
Wire Wire Line
	9500 3700 9500 3950
Connection ~ 9500 3950
Wire Wire Line
	9500 3950 9400 3950
Wire Wire Line
	9400 3700 9400 3950
Connection ~ 9400 3950
Wire Wire Line
	9400 3950 9300 3950
Wire Wire Line
	9300 3700 9300 3950
Connection ~ 9300 3950
Wire Wire Line
	9300 3950 9250 3950
$Comp
L power:+3V3 #PWR01
U 1 1 62838DF3
P 9450 750
F 0 "#PWR01" H 9450 600 50  0001 C CNN
F 1 "+3V3" H 9465 923 50  0000 C CNN
F 2 "" H 9450 750 50  0001 C CNN
F 3 "" H 9450 750 50  0001 C CNN
	1    9450 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 1100 9400 750 
Wire Wire Line
	9400 750  9450 750 
Wire Wire Line
	9500 1100 9500 750 
Wire Wire Line
	9500 750  9450 750 
Connection ~ 9450 750 
Text GLabel 8500 2600 0    50   Output ~ 0
RIGHT_MOTOR_POS_PIN
Text GLabel 8500 2700 0    50   Output ~ 0
RIGHT_MOTOR_NEG_PIN
Text GLabel 8500 2000 0    50   Output ~ 0
RIGHT_MOTOR_SPEED_PIN
Text GLabel 8500 1900 0    50   Output ~ 0
LEFT_MOTOR_POS_PIN
Text GLabel 8500 3100 0    50   Output ~ 0
LEFT_MOTOR_NEG_PIN
Text GLabel 10100 3200 2    50   Output ~ 0
LEFT_MOTOR_SPEED_PIN
Text GLabel 8500 2800 0    50   Output ~ 0
VACUUM_POS_PIN
Text GLabel 8500 2900 0    50   Output ~ 0
VACUUM_NEG_PIN
Text GLabel 8500 1800 0    50   Output ~ 0
POSI_DATA_PIN
Text GLabel 8500 3000 0    50   Output ~ 0
POSI_LATCH_PIN
Text GLabel 8500 2300 0    50   Output ~ 0
POSI_CLOCK_PIN
NoConn ~ 9200 1100
NoConn ~ 9100 1100
NoConn ~ 8500 1500
NoConn ~ 8500 1600
NoConn ~ 8500 2200
NoConn ~ 8500 2400
NoConn ~ 10100 3100
NoConn ~ 10100 2900
NoConn ~ 10100 2800
NoConn ~ 10100 2700
NoConn ~ 10100 2600
NoConn ~ 10100 2500
NoConn ~ 10100 2300
NoConn ~ 10100 2200
NoConn ~ 10100 2100
NoConn ~ 10100 1900
NoConn ~ 10100 1800
NoConn ~ 10100 1600
NoConn ~ 10100 1500
$Comp
L 74xx:74HC595 U2
U 1 1 62847338
P 1800 3000
F 0 "U2" H 2050 3650 50  0000 C CNN
F 1 "74HC595" H 2050 3550 50  0000 C CNN
F 2 "" H 1800 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 1800 3000 50  0001 C CNN
	1    1800 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC595 U6
U 1 1 62849D1F
P 1850 6650
F 0 "U6" H 2100 7300 50  0000 C CNN
F 1 "74HC595" H 2100 7200 50  0000 C CNN
F 2 "" H 1850 6650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 1850 6650 50  0001 C CNN
	1    1850 6650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR04
U 1 1 62850B2F
P 1800 2200
F 0 "#PWR04" H 1800 2050 50  0001 C CNN
F 1 "+3V3" H 1815 2373 50  0000 C CNN
F 2 "" H 1800 2200 50  0001 C CNN
F 3 "" H 1800 2200 50  0001 C CNN
	1    1800 2200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR015
U 1 1 62851C74
P 1850 5900
F 0 "#PWR015" H 1850 5750 50  0001 C CNN
F 1 "+3V3" H 1865 6073 50  0000 C CNN
F 2 "" H 1850 5900 50  0001 C CNN
F 3 "" H 1850 5900 50  0001 C CNN
	1    1850 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 5900 1850 6050
Wire Wire Line
	1800 2200 1800 2400
$Comp
L power:GND #PWR07
U 1 1 628563BE
P 1800 3900
F 0 "#PWR07" H 1800 3650 50  0001 C CNN
F 1 "GND" H 1805 3727 50  0000 C CNN
F 2 "" H 1800 3900 50  0001 C CNN
F 3 "" H 1800 3900 50  0001 C CNN
	1    1800 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3700 1800 3750
$Comp
L power:GND #PWR020
U 1 1 62856D31
P 1850 7500
F 0 "#PWR020" H 1850 7250 50  0001 C CNN
F 1 "GND" H 1855 7327 50  0000 C CNN
F 2 "" H 1850 7500 50  0001 C CNN
F 3 "" H 1850 7500 50  0001 C CNN
	1    1850 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 7350 1850 7400
$Comp
L power:+3V3 #PWR05
U 1 1 6285A3B1
P 600 2250
F 0 "#PWR05" H 600 2100 50  0001 C CNN
F 1 "+3V3" H 615 2423 50  0000 C CNN
F 2 "" H 600 2250 50  0001 C CNN
F 3 "" H 600 2250 50  0001 C CNN
	1    600  2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 2900 600  2900
Wire Wire Line
	600  2900 600  2250
Wire Wire Line
	1400 3200 1400 3750
Wire Wire Line
	1400 3750 1800 3750
Connection ~ 1800 3750
Wire Wire Line
	1800 3750 1800 3900
Text GLabel 1400 3100 0    50   Input ~ 0
POSI_LATCH_PIN
Text GLabel 1400 2800 0    50   Input ~ 0
POSI_CLOCK_PIN
Text GLabel 1400 2600 0    50   Input ~ 0
POSI_DATA_PIN
Text GLabel 1450 6450 0    50   Input ~ 0
POSI_CLOCK_PIN
Text GLabel 1450 6750 0    50   Input ~ 0
POSI_LATCH_PIN
Wire Wire Line
	1450 6850 1450 7400
Wire Wire Line
	1450 7400 1850 7400
Connection ~ 1850 7400
Wire Wire Line
	1850 7400 1850 7500
$Comp
L power:+3V3 #PWR014
U 1 1 6287D4A7
P 650 5900
F 0 "#PWR014" H 650 5750 50  0001 C CNN
F 1 "+3V3" H 665 6073 50  0000 C CNN
F 2 "" H 650 5900 50  0001 C CNN
F 3 "" H 650 5900 50  0001 C CNN
	1    650  5900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D8
U 1 1 62882924
P 2850 3300
F 0 "D8" H 2850 3093 50  0000 C CNN
F 1 "LED_Small" H 2850 3184 50  0000 C CNN
F 2 "" V 2850 3300 50  0001 C CNN
F 3 "~" V 2850 3300 50  0001 C CNN
	1    2850 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D7
U 1 1 628A87BE
P 2850 2950
F 0 "D7" H 2850 2743 50  0000 C CNN
F 1 "LED_Small" H 2850 2834 50  0000 C CNN
F 2 "" V 2850 2950 50  0001 C CNN
F 3 "~" V 2850 2950 50  0001 C CNN
	1    2850 2950
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D6
U 1 1 628A8D2C
P 2850 2600
F 0 "D6" H 2850 2393 50  0000 C CNN
F 1 "LED_Small" H 2850 2484 50  0000 C CNN
F 2 "" V 2850 2600 50  0001 C CNN
F 3 "~" V 2850 2600 50  0001 C CNN
	1    2850 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D5
U 1 1 628A9258
P 2850 2250
F 0 "D5" H 2850 2043 50  0000 C CNN
F 1 "LED_Small" H 2850 2134 50  0000 C CNN
F 2 "" V 2850 2250 50  0001 C CNN
F 3 "~" V 2850 2250 50  0001 C CNN
	1    2850 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D4
U 1 1 628AA441
P 2850 1900
F 0 "D4" H 2850 1693 50  0000 C CNN
F 1 "LED_Small" H 2850 1784 50  0000 C CNN
F 2 "" V 2850 1900 50  0001 C CNN
F 3 "~" V 2850 1900 50  0001 C CNN
	1    2850 1900
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D3
U 1 1 628AA9B3
P 2850 1550
F 0 "D3" H 2850 1343 50  0000 C CNN
F 1 "LED_Small" H 2850 1434 50  0000 C CNN
F 2 "" V 2850 1550 50  0001 C CNN
F 3 "~" V 2850 1550 50  0001 C CNN
	1    2850 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D2
U 1 1 628BC303
P 2850 1200
F 0 "D2" H 2850 993 50  0000 C CNN
F 1 "LED_Small" H 2850 1084 50  0000 C CNN
F 2 "" V 2850 1200 50  0001 C CNN
F 3 "~" V 2850 1200 50  0001 C CNN
	1    2850 1200
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D1
U 1 1 628BC8DA
P 2850 850
F 0 "D1" H 2850 643 50  0000 C CNN
F 1 "LED_Small" H 2850 734 50  0000 C CNN
F 2 "" V 2850 850 50  0001 C CNN
F 3 "~" V 2850 850 50  0001 C CNN
	1    2850 850 
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 2600 2250 2600
Wire Wire Line
	2250 2600 2250 850 
Wire Wire Line
	2250 850  2750 850 
Wire Wire Line
	2750 1200 2300 1200
Wire Wire Line
	2300 1200 2300 2700
Wire Wire Line
	2300 2700 2200 2700
Wire Wire Line
	2200 2800 2350 2800
Wire Wire Line
	2350 2800 2350 1550
Wire Wire Line
	2350 1550 2750 1550
Wire Wire Line
	2750 1900 2400 1900
Wire Wire Line
	2400 1900 2400 2900
Wire Wire Line
	2400 2900 2200 2900
Wire Wire Line
	2200 3000 2450 3000
Wire Wire Line
	2450 3000 2450 2250
Wire Wire Line
	2450 2250 2750 2250
Wire Wire Line
	2750 2600 2500 2600
Wire Wire Line
	2500 2600 2500 3100
Wire Wire Line
	2500 3100 2200 3100
Wire Wire Line
	2200 3200 2550 3200
Wire Wire Line
	2550 3200 2550 2950
Wire Wire Line
	2550 2950 2750 2950
Wire Wire Line
	2200 3300 2750 3300
Wire Wire Line
	650  5900 650  6550
Wire Wire Line
	650  6550 1450 6550
$Comp
L Device:LED_Small D16
U 1 1 62991928
P 2900 6950
F 0 "D16" H 2900 6743 50  0000 C CNN
F 1 "LED_Small" H 2900 6834 50  0000 C CNN
F 2 "" V 2900 6950 50  0001 C CNN
F 3 "~" V 2900 6950 50  0001 C CNN
	1    2900 6950
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D15
U 1 1 6299192E
P 2900 6600
F 0 "D15" H 2900 6393 50  0000 C CNN
F 1 "LED_Small" H 2900 6484 50  0000 C CNN
F 2 "" V 2900 6600 50  0001 C CNN
F 3 "~" V 2900 6600 50  0001 C CNN
	1    2900 6600
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D14
U 1 1 62991934
P 2900 6250
F 0 "D14" H 2900 6043 50  0000 C CNN
F 1 "LED_Small" H 2900 6134 50  0000 C CNN
F 2 "" V 2900 6250 50  0001 C CNN
F 3 "~" V 2900 6250 50  0001 C CNN
	1    2900 6250
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D13
U 1 1 6299193A
P 2900 5900
F 0 "D13" H 2900 5693 50  0000 C CNN
F 1 "LED_Small" H 2900 5784 50  0000 C CNN
F 2 "" V 2900 5900 50  0001 C CNN
F 3 "~" V 2900 5900 50  0001 C CNN
	1    2900 5900
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D12
U 1 1 62991940
P 2900 5550
F 0 "D12" H 2900 5343 50  0000 C CNN
F 1 "LED_Small" H 2900 5434 50  0000 C CNN
F 2 "" V 2900 5550 50  0001 C CNN
F 3 "~" V 2900 5550 50  0001 C CNN
	1    2900 5550
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D11
U 1 1 62991946
P 2900 5200
F 0 "D11" H 2900 4993 50  0000 C CNN
F 1 "LED_Small" H 2900 5084 50  0000 C CNN
F 2 "" V 2900 5200 50  0001 C CNN
F 3 "~" V 2900 5200 50  0001 C CNN
	1    2900 5200
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D10
U 1 1 6299194C
P 2900 4850
F 0 "D10" H 2900 4643 50  0000 C CNN
F 1 "LED_Small" H 2900 4734 50  0000 C CNN
F 2 "" V 2900 4850 50  0001 C CNN
F 3 "~" V 2900 4850 50  0001 C CNN
	1    2900 4850
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D9
U 1 1 62991952
P 2900 4500
F 0 "D9" H 2900 4293 50  0000 C CNN
F 1 "LED_Small" H 2900 4384 50  0000 C CNN
F 2 "" V 2900 4500 50  0001 C CNN
F 3 "~" V 2900 4500 50  0001 C CNN
	1    2900 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	2250 6250 2300 6250
Wire Wire Line
	2300 6250 2300 4500
Wire Wire Line
	2300 4500 2800 4500
Wire Wire Line
	2800 4850 2350 4850
Wire Wire Line
	2350 4850 2350 6350
Wire Wire Line
	2350 6350 2250 6350
Wire Wire Line
	2250 6450 2400 6450
Wire Wire Line
	2400 6450 2400 5200
Wire Wire Line
	2400 5200 2800 5200
Wire Wire Line
	2800 5550 2450 5550
Wire Wire Line
	2450 5550 2450 6550
Wire Wire Line
	2450 6550 2250 6550
Wire Wire Line
	2250 6650 2500 6650
Wire Wire Line
	2500 6650 2500 5900
Wire Wire Line
	2500 5900 2800 5900
Wire Wire Line
	2800 6250 2550 6250
Wire Wire Line
	2550 6250 2550 6750
Wire Wire Line
	2550 6750 2250 6750
Wire Wire Line
	2250 6850 2600 6850
Wire Wire Line
	2600 6850 2600 6600
Wire Wire Line
	2600 6600 2800 6600
Wire Wire Line
	2250 6950 2800 6950
$Comp
L power:GND #PWR019
U 1 1 6299196E
P 3200 7100
F 0 "#PWR019" H 3200 6850 50  0001 C CNN
F 1 "GND" H 3205 6927 50  0000 C CNN
F 2 "" H 3200 7100 50  0001 C CNN
F 3 "" H 3200 7100 50  0001 C CNN
	1    3200 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4500 3200 4500
Wire Wire Line
	3200 4500 3200 4850
Wire Wire Line
	3000 6950 3200 6950
Connection ~ 3200 6950
Wire Wire Line
	3200 6950 3200 7100
Wire Wire Line
	3000 6600 3200 6600
Connection ~ 3200 6600
Wire Wire Line
	3200 6600 3200 6950
Wire Wire Line
	3000 6250 3200 6250
Connection ~ 3200 6250
Wire Wire Line
	3200 6250 3200 6600
Wire Wire Line
	3000 5900 3200 5900
Connection ~ 3200 5900
Wire Wire Line
	3200 5900 3200 6250
Wire Wire Line
	3000 5550 3200 5550
Connection ~ 3200 5550
Wire Wire Line
	3200 5550 3200 5900
Wire Wire Line
	3000 5200 3200 5200
Connection ~ 3200 5200
Wire Wire Line
	3200 5200 3200 5550
Wire Wire Line
	3000 4850 3200 4850
Connection ~ 3200 4850
Wire Wire Line
	3200 4850 3200 5200
Wire Wire Line
	3200 4500 3200 3300
Connection ~ 3200 4500
Wire Wire Line
	2950 850  3200 850 
Wire Wire Line
	2950 3300 3200 3300
Connection ~ 3200 3300
Wire Wire Line
	3200 3300 3200 2950
Wire Wire Line
	2950 2950 3200 2950
Connection ~ 3200 2950
Wire Wire Line
	3200 2950 3200 2600
Wire Wire Line
	2950 2600 3200 2600
Connection ~ 3200 2600
Wire Wire Line
	3200 2600 3200 2250
Wire Wire Line
	2950 2250 3200 2250
Connection ~ 3200 2250
Wire Wire Line
	3200 2250 3200 1900
Wire Wire Line
	2950 1900 3200 1900
Connection ~ 3200 1900
Wire Wire Line
	3200 1900 3200 1550
Wire Wire Line
	2950 1550 3200 1550
Connection ~ 3200 1550
Wire Wire Line
	3200 1550 3200 1200
Wire Wire Line
	2950 1200 3200 1200
Connection ~ 3200 1200
Wire Wire Line
	3200 1200 3200 850 
NoConn ~ 2250 7150
Wire Wire Line
	2200 3500 2350 3500
Wire Wire Line
	2350 3500 2350 4300
Wire Wire Line
	2350 4300 1200 4300
Wire Wire Line
	1200 4300 1200 6250
Wire Wire Line
	1200 6250 1450 6250
$Comp
L domestik-drone:PowerBoost_1000C U5
U 1 1 629FA0C8
P 10000 5650
F 0 "U5" H 10494 5696 50  0000 L CNN
F 1 "PowerBoost_1000C" H 10494 5605 50  0000 L CNN
F 2 "" H 10000 5650 50  0001 C CNN
F 3 "" H 10000 5650 50  0001 C CNN
	1    10000 5650
	1    0    0    -1  
$EndComp
NoConn ~ 10450 5650
NoConn ~ 9550 5650
$Comp
L power:GND #PWR016
U 1 1 62A04AE6
P 10000 6250
F 0 "#PWR016" H 10000 6000 50  0001 C CNN
F 1 "GND" H 10005 6077 50  0000 C CNN
F 2 "" H 10000 6250 50  0001 C CNN
F 3 "" H 10000 6250 50  0001 C CNN
	1    10000 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 6050 9850 6250
Wire Wire Line
	9850 6250 10000 6250
Wire Wire Line
	10150 6050 10150 6250
Wire Wire Line
	10150 6250 10000 6250
Connection ~ 10000 6250
Wire Wire Line
	9900 5050 9900 5250
NoConn ~ 9750 5250
NoConn ~ 10100 5250
NoConn ~ 10250 5250
$Comp
L power:+BATT #PWR011
U 1 1 62A3AFC2
P 9900 5050
F 0 "#PWR011" H 9900 4900 50  0001 C CNN
F 1 "+BATT" H 9915 5223 50  0000 C CNN
F 2 "" H 9900 5050 50  0001 C CNN
F 3 "" H 9900 5050 50  0001 C CNN
	1    9900 5050
	1    0    0    -1  
$EndComp
$Comp
L domestik-drone:TB6612_Breakout_Driver U1
U 1 1 62A47E2D
P 5300 2200
F 0 "U1" H 5900 3150 50  0000 C CNN
F 1 "TB6612_Breakout_Driver" H 6300 3000 50  0000 C CNN
F 2 "" H 5350 2750 50  0001 C CNN
F 3 "" H 5350 2750 50  0001 C CNN
	1    5300 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 62A48707
P 5300 3400
F 0 "#PWR06" H 5300 3150 50  0001 C CNN
F 1 "GND" H 5305 3227 50  0000 C CNN
F 2 "" H 5300 3400 50  0001 C CNN
F 3 "" H 5300 3400 50  0001 C CNN
	1    5300 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3000 5050 3400
Wire Wire Line
	5050 3400 5300 3400
Wire Wire Line
	5550 3000 5550 3400
Wire Wire Line
	5550 3400 5300 3400
Connection ~ 5300 3400
Wire Wire Line
	5300 3000 5300 3400
$Comp
L power:+3V3 #PWR02
U 1 1 62A58173
P 5100 1250
F 0 "#PWR02" H 5100 1100 50  0001 C CNN
F 1 "+3V3" H 5115 1423 50  0000 C CNN
F 2 "" H 5100 1250 50  0001 C CNN
F 3 "" H 5100 1250 50  0001 C CNN
	1    5100 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1250 5100 1500
$Comp
L power:+BATT #PWR03
U 1 1 62A5DF92
P 5600 1250
F 0 "#PWR03" H 5600 1100 50  0001 C CNN
F 1 "+BATT" H 5615 1423 50  0000 C CNN
F 2 "" H 5600 1250 50  0001 C CNN
F 3 "" H 5600 1250 50  0001 C CNN
	1    5600 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1500 5600 1250
NoConn ~ 4700 2200
$Comp
L Motor:Motor_DC_ALT M1
U 1 1 62A68B05
P 6250 1750
F 0 "M1" H 6397 1746 50  0000 L CNN
F 1 "Motor_DC_ALT" H 6397 1655 50  0000 L CNN
F 2 "" H 6250 1660 50  0001 C CNN
F 3 "~" H 6250 1660 50  0001 C CNN
	1    6250 1750
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_DC_ALT M2
U 1 1 62A694D9
P 6250 2550
F 0 "M2" H 6397 2546 50  0000 L CNN
F 1 "Motor_DC_ALT" H 6397 2455 50  0000 L CNN
F 2 "" H 6250 2460 50  0001 C CNN
F 3 "~" H 6250 2460 50  0001 C CNN
	1    6250 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2050 5950 2050
Wire Wire Line
	5950 1850 6050 1850
Wire Wire Line
	6050 1850 6050 1550
Wire Wire Line
	6050 1550 6250 1550
Wire Wire Line
	6250 2350 5950 2350
Wire Wire Line
	5950 2550 6050 2550
Wire Wire Line
	6050 2550 6050 2850
Wire Wire Line
	6050 2850 6250 2850
Text GLabel 4700 1900 0    50   Input ~ 0
LEFT_MOTOR_POS_PIN
Text GLabel 4700 2050 0    50   Input ~ 0
LEFT_MOTOR_NEG_PIN
Text GLabel 4700 1750 0    50   Input ~ 0
LEFT_MOTOR_SPEED_PIN
Text GLabel 4700 2500 0    50   Input ~ 0
RIGHT_MOTOR_POS_PIN
Text GLabel 4700 2650 0    50   Input ~ 0
RIGHT_MOTOR_NEG_PIN
Text GLabel 4700 2350 0    50   Input ~ 0
RIGHT_MOTOR_SPEED_PIN
$Comp
L domestik-drone:TB6612_Breakout_Driver U3
U 1 1 62B0B160
P 5100 5500
F 0 "U3" H 5700 6450 50  0000 C CNN
F 1 "TB6612_Breakout_Driver" H 6100 6300 50  0000 C CNN
F 2 "" H 5150 6050 50  0001 C CNN
F 3 "" H 5150 6050 50  0001 C CNN
	1    5100 5500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR09
U 1 1 62B129C9
P 4900 4450
F 0 "#PWR09" H 4900 4300 50  0001 C CNN
F 1 "+3V3" H 4915 4623 50  0000 C CNN
F 2 "" H 4900 4450 50  0001 C CNN
F 3 "" H 4900 4450 50  0001 C CNN
	1    4900 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4450 4900 4650
$Comp
L power:+15V #PWR010
U 1 1 62B19DBA
P 5400 4450
F 0 "#PWR010" H 5400 4300 50  0001 C CNN
F 1 "+15V" H 5415 4623 50  0000 C CNN
F 2 "" H 5400 4450 50  0001 C CNN
F 3 "" H 5400 4450 50  0001 C CNN
	1    5400 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4450 5400 4800
NoConn ~ 4500 5950
NoConn ~ 4500 5800
NoConn ~ 4500 5650
NoConn ~ 4500 5500
$Comp
L power:GND #PWR018
U 1 1 62B38261
P 5100 6600
F 0 "#PWR018" H 5100 6350 50  0001 C CNN
F 1 "GND" H 5105 6427 50  0000 C CNN
F 2 "" H 5100 6600 50  0001 C CNN
F 3 "" H 5100 6600 50  0001 C CNN
	1    5100 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6300 4850 6600
Wire Wire Line
	4850 6600 5100 6600
Wire Wire Line
	5350 6300 5350 6600
Wire Wire Line
	5350 6600 5100 6600
Connection ~ 5100 6600
Wire Wire Line
	5100 6300 5100 6600
NoConn ~ 5750 5850
NoConn ~ 5750 5650
$Comp
L Motor:Fan_ALT M3
U 1 1 62B55649
P 6300 5450
F 0 "M3" H 6510 5496 50  0000 L CNN
F 1 "Fan_ALT" H 6510 5405 50  0000 L CNN
F 2 "" H 6300 5400 50  0001 C CNN
F 3 "~" H 6300 5400 50  0001 C CNN
	1    6300 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 5150 5750 5150
Wire Wire Line
	5750 5350 6000 5350
Wire Wire Line
	6000 5350 6000 5750
Wire Wire Line
	6000 5750 6300 5750
Wire Wire Line
	4500 5050 4500 4650
Wire Wire Line
	4500 4650 4900 4650
Connection ~ 4900 4650
Wire Wire Line
	4900 4650 4900 4800
Text GLabel 4500 5200 0    50   Input ~ 0
VACUUM_POS_PIN
Text GLabel 4500 5350 0    50   Input ~ 0
VACUUM_NEG_PIN
$Comp
L domestik-drone:mEZD41501A-C U4
U 1 1 62BA5F06
P 8250 5650
F 0 "U4" H 8250 6115 50  0000 C CNN
F 1 "mEZD41501A-C" H 8250 6024 50  0000 C CNN
F 2 "" H 8250 5650 50  0001 C CNN
F 3 "" H 8250 5650 50  0001 C CNN
	1    8250 5650
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR012
U 1 1 62BB04CB
P 7450 5100
F 0 "#PWR012" H 7450 4950 50  0001 C CNN
F 1 "+BATT" H 7465 5273 50  0000 C CNN
F 2 "" H 7450 5100 50  0001 C CNN
F 3 "" H 7450 5100 50  0001 C CNN
	1    7450 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 5100 7450 5500
Wire Wire Line
	7450 5500 7700 5500
Wire Wire Line
	7700 5800 7450 5800
Wire Wire Line
	7450 5800 7450 5500
Connection ~ 7450 5500
$Comp
L power:GND #PWR017
U 1 1 62BBC8B0
P 8300 6300
F 0 "#PWR017" H 8300 6050 50  0001 C CNN
F 1 "GND" H 8305 6127 50  0000 C CNN
F 2 "" H 8300 6300 50  0001 C CNN
F 3 "" H 8300 6300 50  0001 C CNN
	1    8300 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 6050 8100 6300
Wire Wire Line
	8100 6300 8300 6300
Wire Wire Line
	8450 6050 8450 6300
Wire Wire Line
	8450 6300 8300 6300
Connection ~ 8300 6300
$Comp
L power:+15V #PWR013
U 1 1 62BCA541
P 8950 5100
F 0 "#PWR013" H 8950 4950 50  0001 C CNN
F 1 "+15V" H 8965 5273 50  0000 C CNN
F 2 "" H 8950 5100 50  0001 C CNN
F 3 "" H 8950 5100 50  0001 C CNN
	1    8950 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 5100 8950 5500
Wire Wire Line
	8950 5500 8800 5500
Wire Wire Line
	8950 5500 8950 5800
Wire Wire Line
	8950 5800 8800 5800
Connection ~ 8950 5500
Wire Notes Line
	3550 7800 3550 500 
Wire Notes Line
	7150 450  7150 6550
Wire Notes Line
	7150 4450 11250 4450
Text Notes 600  650  0    100  ~ 0
LEDs
Text Notes 3650 700  0    100  ~ 0
Motors
Text Notes 7250 750  0    100  ~ 0
Raspberry Pi
Text Notes 7250 4650 0    100  ~ 0
Power
$EndSCHEMATC
