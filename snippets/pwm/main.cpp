/*
* Based on the pwm example in the BCM2835 C library
* Connect an LED across pin 12 (GPIO 18 on a raspberry pi) and ground to watch it fade.
* NOTE: Audio must be disabled on the Pi and the compiled code must be run with access to /dev/gpiomem.
* The easiest way to achieve this is to run the compiled code as root.
*/
#include <bcm2835.h>
#include <stdio.h>

#define PIN RPI_GPIO_P1_12
#define PWM_CHANNEL 0
#define RANGE 1024

int main(int argc, char **argv)
{
    if (!bcm2835_init())
	return 1;

    bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_ALT5);

    // In MARKSPACE mode,  the pulse repetition frequency will be 1.2MHz/1024 = 1171.875Hz
    bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_16);
    bcm2835_pwm_set_mode(PWM_CHANNEL, 1, 1);
    bcm2835_pwm_set_range(PWM_CHANNEL, RANGE);

    int direction = 1; // 1 is increase, -1 is decrease
    int data = 1;
    while (1)
    {
	if (data == 1)
	    direction = 1;   // Switch to increasing
	else if (data == RANGE-1)
	    direction = -1;  // Switch to decreasing
	data += direction;
	bcm2835_pwm_set_data(PWM_CHANNEL, data);
	bcm2835_delay(1);
    }

    bcm2835_close();
    return 0;
}
