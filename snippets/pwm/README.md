# How to run
Compile and run the code with `./compile.sh` and `./pwm` in the command line.

## Note
- The [BCM2835 C library](http://www.airspayce.com/mikem/bcm2835/) must be installed.
- Audio must be disabled on a Raspberry PI for PWM to work with the above library.
- The user running this code must have access to `/dev/gpiomem` for PWM to work. `sudo ./pwm` will suffice.
