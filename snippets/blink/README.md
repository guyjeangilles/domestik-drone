# How to run
Compile and run the code with `./compile.sh` and `./blink` in the command line. Note the [BCM2835 C library](http://www.airspayce.com/mikem/bcm2835/) must be installed.
