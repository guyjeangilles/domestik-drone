/*
* Based on the blink example from the BCM2835 C Library.
* Connect an LED across GPIO pin 18 and ground and watch it blink.
*/
#include <bcm2835.h>
#include <stdio.h>

#define PIN RPI_GPIO_P1_12 // GPIO pin 18

int main(int argc, char **argv)
{
    // exit if initialization was not successful
    if (!bcm2835_init())
      return 1;

    // Set the pin to be an output
    bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_OUTP);

    // Blink
    while (1)
    {
	bcm2835_gpio_write(PIN, HIGH);
	bcm2835_delay(500);
	bcm2835_gpio_write(PIN, LOW);
	bcm2835_delay(500);
    }
    bcm2835_close();
    return 0;
}
