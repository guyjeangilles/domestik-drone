# How to Build
Run `make` in the command line to build the code, then execute.
The [RPLidar SDK](https://github.com/Slamtec/rplidar_sdk) is required to build.
