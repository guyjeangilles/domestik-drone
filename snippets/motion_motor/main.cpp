/*
* Hardware test for the motion motors
* Connect an h-bridge to two motors.
* Then connect the h-bridge input pins to GPIO 17, 27, 22, 23.
* Connect the h-brdige VCC/Vm to 5V and the enable/PWM pins to 3v3.
*/
#include <bcm2835.h>
#define LEFT_MOTOR_POS_PIN RPI_BPLUS_GPIO_J8_11 // GPIO 17
#define LEFT_MOTOR_NEG_PIN RPI_BPLUS_GPIO_J8_13 // GPIO 27
#define RIGHT_MOTOR_POS_PIN RPI_BPLUS_GPIO_J8_15 // GPIO 22
#define RIGHT_MOTOR_NEG_PIN RPI_BPLUS_GPIO_J8_16 // GPIO 23

int main(int argc, char **argv){
	if (!bcm2835_init())
		return 1; // exit if code can't access GPIO pins

	// Setup motor direction control
	bcm2835_gpio_fsel(LEFT_MOTOR_POS_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(LEFT_MOTOR_NEG_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(RIGHT_MOTOR_POS_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(RIGHT_MOTOR_NEG_PIN, BCM2835_GPIO_FSEL_OUTP);

	while(1)
	{
		// move forward
		bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, HIGH);
		bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, LOW);
		bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, HIGH);
		bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, LOW);

		bcm2835_delay(1000);

		// move right
		bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, HIGH);
		bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, LOW);
		bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, LOW);
		bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, HIGH);

		bcm2835_delay(1000);

		// move backward
		bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, LOW);
		bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, HIGH);
		bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, LOW);
		bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, HIGH);

		bcm2835_delay(1000);

		// move left
		bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, LOW);
		bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, HIGH);
		bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, HIGH);
		bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, LOW);

		bcm2835_delay(1000);

		// stop
		bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, LOW);
		bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, LOW);
		bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, LOW);
		bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, LOW);

		bcm2835_delay(1000);
	}
	bcm2835_close();
	return 0;
}
