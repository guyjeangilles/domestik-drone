/*
* Hardware test for POSI shift registers
*/
#include <bcm2835.h>
#define POSI_DATA_PIN RPI_BPLUS_GPIO_J8_36 // GPIO 16
#define POSI_LATCH_PIN RPI_BPLUS_GPIO_J8_37 // GPIO 26
#define POSI_CLOCK_PIN RPI_BPLUS_GPIO_J8_38 // GPIO 20

// based on arduino shiftOut
void writePOSIregister(uint8_t dataPin, uint8_t clockPin, bool leastBitFirst, uint8_t val)
{
	uint8_t i;

	for (i = 0; i < 8; i++)
	{
		if (leastBitFirst)
		{
			bcm2835_gpio_write(POSI_DATA_PIN, !!(val & (1 << i)));
		}
		else
		{
			bcm2835_gpio_write(POSI_DATA_PIN, !!(val & (1 << (7 - i))));
		}

		bcm2835_gpio_write(POSI_CLOCK_PIN, HIGH);
		bcm2835_gpio_write(POSI_CLOCK_PIN, LOW);
	}
}

int main(int argc, char **argv){
	if (!bcm2835_init())
		return 1; // exit if code can't access GPIO pins

	// Setup motor direction control
	bcm2835_gpio_fsel(POSI_DATA_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(POSI_LATCH_PIN, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(POSI_CLOCK_PIN, BCM2835_GPIO_FSEL_OUTP);

	uint8_t leds = 0;
	bcm2835_gpio_write(POSI_LATCH_PIN, LOW);
	writePOSIregister(POSI_DATA_PIN, POSI_CLOCK_PIN, 1, leds);
	bcm2835_gpio_write(POSI_LATCH_PIN, HIGH);

	while(1)
	{
		for (uint16_t i = 0; i < 8; i++)
		{
			leds ^= (1 << i);
			bcm2835_gpio_write(POSI_LATCH_PIN, LOW);
			writePOSIregister(POSI_DATA_PIN, POSI_CLOCK_PIN, 1, leds);
			bcm2835_gpio_write(POSI_LATCH_PIN, HIGH);

			bcm2835_delay(1000);
		}
	}
	bcm2835_close();
	return 0;
}
