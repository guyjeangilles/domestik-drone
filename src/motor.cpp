#include <bcm2835.h>
#include "motor.h"

#define RIGHT_MOTOR_POS_PIN RPI_BPLUS_GPIO_J8_15 // GPIO 22
#define RIGHT_MOTOR_NEG_PIN RPI_BPLUS_GPIO_J8_16 // GPIO 23
#define RIGHT_MOTOR_SPEED_PIN RPI_BPLUS_GPIO_J8_12 // GPIO 18
#define RIGHT_MOTOR_PWM_CHANNEL 0
#define RIGHT_MOTOR_PWM_RANGE 1024

#define LEFT_MOTOR_POS_PIN RPI_BPLUS_GPIO_J8_11 // GPIO 17
#define LEFT_MOTOR_NEG_PIN RPI_BPLUS_GPIO_J8_13 // GPIO 27
#define LEFT_MOTOR_SPEED_PIN RPI_BPLUS_GPIO_J8_33 // GPIO 13
#define LEFT_MOTOR_PWM_CHANNEL 1
#define LEFT_MOTOR_PWM_RANGE 1024

#define VACUUM_POS_PIN RPI_BPLUS_GPIO_J8_18 // GPIO 24
#define VACUUM_NEG_PIN RPI_BPLUS_GPIO_J8_22 // GPIO 25

// bcm2835 must be initialized before calling this
void motorInit()
{
    // setup motor polarity pins
    bcm2835_gpio_fsel(LEFT_MOTOR_POS_PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(LEFT_MOTOR_NEG_PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(RIGHT_MOTOR_POS_PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(RIGHT_MOTOR_NEG_PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(VACUUM_POS_PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(VACUUM_NEG_PIN, BCM2835_GPIO_FSEL_OUTP);

    // setup speed control
    bcm2835_gpio_fsel(RIGHT_MOTOR_SPEED_PIN, BCM2835_GPIO_FSEL_ALT5);
    bcm2835_gpio_fsel(LEFT_MOTOR_SPEED_PIN, BCM2835_GPIO_FSEL_ALT0);

    bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_16);
    bcm2835_pwm_set_mode(RIGHT_MOTOR_PWM_CHANNEL, 1, 1);
    bcm2835_pwm_set_mode(LEFT_MOTOR_PWM_CHANNEL, 1, 1);
    bcm2835_pwm_set_range(RIGHT_MOTOR_PWM_CHANNEL, RIGHT_MOTOR_PWM_RANGE);
    bcm2835_pwm_set_range(LEFT_MOTOR_PWM_CHANNEL, LEFT_MOTOR_PWM_RANGE);

    bcm2835_pwm_set_data(RIGHT_MOTOR_PWM_CHANNEL, RIGHT_MOTOR_PWM_RANGE - 1);
    bcm2835_pwm_set_data(LEFT_MOTOR_PWM_CHANNEL, LEFT_MOTOR_PWM_RANGE - 1);
}

// motorInt must be called before calling the direction functions
void motorForward()
{
    bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, HIGH);
    bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, LOW);
    bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, HIGH);
    bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, LOW);
}


void motorRight()
{
    bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, HIGH);
    bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, LOW);
    bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, LOW);
    bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, HIGH);
}


void motorBackward()
{
    bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, LOW);
    bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, HIGH);
    bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, LOW);
    bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, HIGH);

}

void motorLeft()
{
    bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, LOW);
    bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, HIGH);
    bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, HIGH);
    bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, LOW);
}

void motorStop()
{
    bcm2835_gpio_write(LEFT_MOTOR_POS_PIN, LOW);
    bcm2835_gpio_write(LEFT_MOTOR_NEG_PIN, LOW);
    bcm2835_gpio_write(RIGHT_MOTOR_POS_PIN, LOW);
    bcm2835_gpio_write(RIGHT_MOTOR_NEG_PIN, LOW);
}

void vacuumStart()
{
    bcm2835_gpio_write(VACUUM_POS_PIN, HIGH);
    bcm2835_gpio_write(VACUUM_NEG_PIN, LOW);
}

void vacuumStop()
{
    bcm2835_gpio_write(VACUUM_POS_PIN, LOW);
    bcm2835_gpio_write(VACUUM_NEG_PIN, LOW);
}
