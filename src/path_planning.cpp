/* This file really should be called collision avoidance
*  It also sucks don't use it.
*/

#include <array>
#include <algorithm>
#include <cmath>
#include <limits>
#include "path_planning.h"

#define COLLISION_DISTANCE 140 // millimeters

// replace invalid values from raw angle measurements
std::array<float, 360> cleanDistances(std::array<float, 360> distances)
{
    for (uint16_t angle = 0; angle < distances.size(); angle++)
    {
        float distance = distances[angle];
        if (distance == 0 || std::isnan(distance)) distances[angle] = std::numeric_limits<float>::infinity();
    }
    return distances;
}

// divide direction into 90 degree chucks and find the angle that has the closest object
std::array<float, 4> getClosestInQuadrants(std::array<float, 360> distances)
{
    std::array<float, 4> closestAngles;
    float * val;

    std::array<float, 360> cleanDists = cleanDistances(distances);

    // front right
    val = std::min_element(&cleanDists[0], &cleanDists[89]);
    closestAngles[0] = * val;

    // back right
    val = std::min_element(&cleanDists[90], &cleanDists[179]);
    closestAngles[1] = * val;

    // back left
    val = std::min_element(&cleanDists[180], &cleanDists[269]);
    closestAngles[2] = * val;

    // front left
    val = std::min_element(&cleanDists[270], &cleanDists[359]);
    closestAngles[3] = * val;

    return closestAngles;
}

// go forward unless there will be a collision
Direction decideDirection(std::array<float, 360> distances)
{
    Direction direction = Forward;
    std::array<float, 4> closestAngles = getClosestInQuadrants(distances);

    // a collision can still occur moving forward with this logic but eh, ship it
    if (closestAngles[0] < COLLISION_DISTANCE && closestAngles[3] < COLLISION_DISTANCE)
    { // collision will occur if we move forward
        if (closestAngles[0] > COLLISION_DISTANCE && closestAngles[1] > COLLISION_DISTANCE) // move to the right if no collision will occur
        {
            direction = Right;
        }
        else if (closestAngles[2] > COLLISION_DISTANCE && closestAngles[3] > COLLISION_DISTANCE) // move left if no collision will occur
        {
            direction = Left;
        }
        else if (closestAngles[1] > COLLISION_DISTANCE && closestAngles[2] > COLLISION_DISTANCE)  // move backwards if no collision will occur
        {
            direction = Backward;
        }
    }
    return direction;
}
