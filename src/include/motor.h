#ifndef DOMESTIKDRONE_MOTOR_H_
#define DOMESTIKDRONE_MOTOR_H_

void motorInit();

void motorForward();

void motorRight();

void motorBackward();

void motorLeft();

void motorStop();

void vacuumStart();

void vacuumStop();

#endif // DOMESTIKDRONE_MOTOR_H_
