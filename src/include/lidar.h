#ifndef DOMESTIKDRONE_LIDAR_H_
#define DOMESTIKDRONE_LIDAR_H_

#include <array>
#include "rplidar.h"

class Lidar{
    public:
        Lidar(const char * channelName, sl_u32 baudrate);
        ~Lidar();
        void refreshData(void);
        std::array<float, 360> aggregateDistances(); // returns the average distance (mm) for each angle integer

    private:
        sl::ILidarDriver * driver;
        void printDeviceInfo(sl_lidar_response_device_info_t devInfo);

        sl_lidar_response_measurement_node_hq_t nodes[8192];  // raw angle samples
        size_t count;
        void setCount(void);

        float angle(uint16_t index); // degrees
        float distance(uint16_t index); // millimeters
        sl_u8 quality(uint16_t index);  // validity of a raw angle sample
};
#endif  // DOMESTIKDRONE_LIDAR_H_
