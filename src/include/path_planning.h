#ifndef DOMESTIKDRONE_PATHPLANNING_H_
#define DOMESTIKDRONE_PATHPLANNING_H_

#include <array>
#include "rplidar.h"

enum Direction {
    Forward,
    Right,
    Backward,
    Left,
};

Direction decideDirection(std::array<float, 360> distances);

#endif  // DOMESTIKDRONE_PATHPLANNING_H_
