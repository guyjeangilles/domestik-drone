#include <array>
#include <bcm2835.h>
#include "rplidar.h"
#include "lidar.h"
#include "motor.h"
#include "path_planning.h"

void parseCommandLine(int argc, char const * argv[], char const ** channelName_ptr, sl_u32 * baudrate_ptr)
{
    bool channelGiven = 0;
    bool baudrateGiven = 0;
    if (argc > 1)
    {
        channelGiven = 1;
        if (argc - 1 >= 2)
            baudrateGiven = 1;
    }

    if (channelGiven)
    {
        *channelName_ptr = argv[1];
    }
    if (baudrateGiven)
    {
        *baudrate_ptr = strtoul(argv[2], NULL, 10);
    }
}

int main(int argc, char const * argv[])
{
    char const * channelName = NULL;
    sl_u32 baudrate = 0;
    parseCommandLine(argc, argv, &channelName, &baudrate);

    Lidar lidar(channelName, baudrate);

    if (!bcm2835_init()) return 1;
    motorInit();
    vacuumStart();

    while(1)
    {
        lidar.refreshData();
        Direction direction = decideDirection(lidar.aggregateDistances());
        switch (direction)
        {
            case Forward:
                printf("Move forward");
                motorForward();
                break;
            case Right:
                printf("Move right");
                motorRight();
                break;
            case Backward:
                printf("Move backward");
                motorBackward();
                break;
            case Left:
                printf("Move left");
                motorLeft();
                break;
            default:
                printf("Something went wrong deciding direction");
                break;
        }
        printf("\n");
    }
    bcm2835_close();
    return 0;
}
