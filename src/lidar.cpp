#include <cmath>
#include <iostream>
#include <limits>
#include <numeric>
#include <vector>
#include "lidar.h"

#define DEFAULT_LIDAR_BAUDRATE 115200
#define DEFAULT_LIDAR_CHANNEL "/dev/ttyUSB0"

#ifndef _countof
#define _countof(_Array) (int)(sizeof(_Array) / sizeof(_Array[0]))
#endif

void Lidar::refreshData(void)
{
    this->setCount();

    sl_result scan_result = this->driver->grabScanDataHq(this->nodes, this->count);
    if (SL_IS_OK(scan_result))
    {
        this->driver->ascendScanData(this->nodes, this->count);
    }
    else
    {
        printf("Scan result not ok.\n");
    }
}

// return an 360 element array with the average distance, in mm, for each angles
std::array<float, 360> Lidar::aggregateDistances()
{
    int angle = 0;
    int pos = 0;
    std::array<float, 360> distances;

    while (pos < (int)this->count)
    {
        if (this->quality(pos) == 0)
        {
            distances[angle] = std::numeric_limits<float>::quiet_NaN();
            pos++;
            angle = (int)this->angle(pos);
            continue;
        }

        std::vector<float> dists;
        while(angle == (int)this->angle(pos))
        {
            float d = this->distance(pos);
            pos++;
            if (d != 0.f) dists.push_back(d);
        }

        distances[angle] = std::accumulate(dists.begin(), dists.end(), 0.0f) / dists.size();
        angle++;

       if (angle >= 360) break;
    }
    return distances;
}

float Lidar::angle(uint16_t index)
{
    return this->nodes[index].angle_z_q14 * 90.f / 16384.f;
}

float Lidar::distance(uint16_t index)
{
    return this->nodes[index].dist_mm_q2 / 4.0f;
}

sl_u8 Lidar::quality(uint16_t index)
{
    return this->nodes[index].quality >> SL_LIDAR_RESP_MEASUREMENT_QUALITY_SHIFT;
}

void Lidar::printDeviceInfo(sl_lidar_response_device_info_t devInfo)
{
    printf("Slamtec Lidar S/N: ");
    for (int pos = 0; pos < 16; pos++)
    {
        printf("%02X", devInfo.serialnum[pos]);
    }
    printf("\n"
           "Firmware Version: %d.%02d\n"
           "Hardware Revision: %d\n"
           , devInfo.firmware_version>>8
           , devInfo.firmware_version & 0xFF
           , (int)devInfo.hardware_version);
}

Lidar::~Lidar(void)
{
    delete this->driver;
    this->driver = NULL;
}

Lidar::Lidar(const char * channelName, sl_u32 baudrate)
{
    if (channelName == NULL)
    {
        channelName = DEFAULT_LIDAR_CHANNEL;
        printf("Using default Lidar channel\n");
    }
    if (baudrate == 0)
    {
        baudrate = DEFAULT_LIDAR_BAUDRATE;
        printf("Using default Lidar baudrate\n");
    }

    printf("Attempting to connect to device at %s with baudrate: %d\n", channelName, baudrate);
    this->driver = * sl::createLidarDriver();
    if (!this->driver)
    {
        printf("Failed to construct RPLidar driver. Likely insufficient memory. Exitng.\n");
        exit(EXIT_FAILURE);
    }

    sl_result result;
    sl::IChannel * channel = * sl::createSerialPortChannel(channelName, baudrate);
    sl_lidar_response_device_info_t devInfo;
    bool connectSuccess = false;
    result = this->driver->connect(channel);
    if (SL_IS_OK(result))
    {
        result = this->driver->getDeviceInfo(devInfo);
        if (SL_IS_OK(result))
        {
            connectSuccess = true;
            this->printDeviceInfo(devInfo);
        }
    }

    if (!connectSuccess)
    {
         printf("Failed to conenct to device at %s\n", channelName);
         exit(EXIT_FAILURE);
    }
    else
    {
        printf("Connection succesful\n");
    }

    this->driver->setMotorSpeed();
    this->driver->startScan(0,1);
}


void Lidar::setCount(void)
{
    this->count = _countof(this->nodes);
}
